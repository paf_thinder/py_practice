# z - position of mouse 
# x - pos. of cat a 
# y - pos. of cat b 

def catAndMouse(x, y, z):
    if (abs(x - z) == abs(y - z)):
        return 'Mouse C'
    
    if (abs(x - z) < abs(y - z)):
        return 'Cat A'
    else:
        return 'Cat B'
    

