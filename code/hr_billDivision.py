# if bill split correctly - "Bon Appetit"
# otherwise - print the amount of money Bill owes Anna 

# bill - array of integers representing price of each meal 
# k - item Anna doesnt eat 
# b - the amount of money Anna contributed to the bill 

def bonAppetit(bill, k, b):
    bill.remove(bill[k])
    sum_of_the_bill = sum(bill)
    annas_part = int(sum_of_the_bill / 2 )

    if annas_part == b:
        print ("Bon Appetit")
    else:
        print (b - annas_part)