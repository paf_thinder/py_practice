def check_column(s, altered_column):
    return s[0][altered_column] + s[1][altered_column] + s[2][altered_column] == 15
def check_row(s, altered_row):
    return s[altered_row][0] + s[altered_row][1] + s[altered_row][2] == 15
def check_diags(s):
    cond1 = s[0][0] + s[1][1] + s[2][2] == 15 
    cond2 = s[0][2] + s[1][1] + s[2][0] == 15
    return cond1 and cond2

# 1 nr to be changed 
s3 = [[4, 9, 2], 
      [3, 5, 7], 
      [8, 1, 5]]

# 2 nrs to be changed
s4 = []

# 3 nrs to be changed 
s5 = [] 

numbers = []
to_remove = []  # repeated elements
for row in s3:
    for el in row:
        if el not in numbers: 
            numbers.append(el)
        else: 
            to_remove.append(el)

full_set = set([x for x in range(1, 10)])
missing_nrs = list(full_set.difference(set(numbers)))  # missing_nrs is a list so it is iterable

cost = 0 
# CASE 1: 1 nr to be changed 
for i in range(3):
    for j in range(3):
        if s3[i][j] in to_remove:
            backup = s3[i][j]
            s3[i][j] = missing_nrs[0]
            if not (check_column(s3, j) and check_row(s3, i) and check_diags(s3)):  # jeżeli ta liczba tutaj nie pasuje to ; albo spróbuj kolejną, albo sprobuj ją w innym miejscu
                s3[i][j] = backup
            else:
                cost += abs(backup - missing_nrs[0])

print(s3)
print(cost)

# CASE 2: 2 nrs to be changed 
# TODO - popatrz w komentarze i wróć tutaj... innej opcji nie widzę :) 


############

# print(len(to_remove) == len(missing_nrs))
# jaka jest liczba to_remove - takie same są długości tych list
# jaka jest liczba missing_nrs - takie same są długości tych list 

# JAK MI SIĘ TO UDA ZROBIĆ TO BĘDZIE JAKIŚ NEXT LEVEL 

# - musi sobie lecieć element po elemencie 
# - jeżeli ten element jest do wymiany - nie ma go w nrs_set - spróbuj podmienić z pierwszym z missing_nrs <- TODO ; to nie tak, jak wyczaić, że el jest zdublowany
# a więc do wymiany? TODO

# - teraz sprawdzamy warunki magic square'a ; 
    # i tutaj zaczyna się magia :) 
    # tutaj muszę się zatrzymać teraz i lepiej to rozkminić.  

        # suma elementów w rzędzie == 15 
        # suma elementów w kolumnie == 15 
        # suma elementów po 1 przekątnej == 15 
        # suma elementów po 2 przekątnej == 15 
        # suma elementów w całym kwadracie == 45