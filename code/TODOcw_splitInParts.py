#!/bin/python3

# my solution 
def split_in_parts(s, part_length): 
    ret = ""
    low_bound = 0 
    upp_bound = part_length 
    
    for i in range(0, len(s), part_length):
        ret += s[low_bound:upp_bound]
        ret += " "
        low_bound = upp_bound
        upp_bound = upp_bound + part_length 
        
    return ret[:-1]

    ''' 
    TESTS 

@test.describe('Example Tests')
def example_tests():
    test.assert_equals(split_in_parts("supercalifragilisticexpialidocious", 3), "sup erc ali fra gil ist ice xpi ali doc iou s")
    test.assert_equals(split_in_parts("HelloKata", 1), "H e l l o K a t a")
    test.assert_equals(split_in_parts("HelloKata", 9), "HelloKata")

    '''

# other solutions 
from textwrap import wrap

def split_in_parts(s, part_length):
    return " ".join(wrap(s, part_length)) # no idea what wrap does ; what kind of library is that? 

### 

# TODO ; comprehend 
# THIS IS SOLUTION I LIKE A LOT 
def split_in_parts(s, part_length):
    return " ".join([s[i:i+part_length] for i in range(0, len(s), part_length)])

