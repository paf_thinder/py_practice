# dictionaries are indexed by KEY 

my_cars = {
            "name": "Volkswagen",
            "model": "Golf",
            "YOP": 2018
          }

print(my_cars) # prints whole dictionary 
print(my_cars["name"]) # Volkswagen 

print("The car I currently posses is {} {} which was produced in {}. Not bad, huh?".format(my_cars["name"], 
                                                                                           my_cars["model"],
                                                                                           my_cars["YOP"]))



# let's create a template, frame of a dictionary, and fill it afterwards 
my_achievements = {
                   'name': '',
                   'year of completing': '',
                   'importance': 0,
                   'additional info': ''  
                  }

print(my_achievements)
print("...not too much... YET!\n")

# let's fill a dictionary with some data 
my_achievements['name'] = 'GETTING THE JOB IN THE ZF'
my_achievements['year of completing'] = '2020'
my_achievements['importance'] = 8 # out of 10 
my_achievements['additional info'] = 'this is how I started to shape the world ; and my future, obviously'

print(my_achievements)

print("#### ANOTHER PROGRAM #### ")

vowels = ['a', 'e', 'i', 'o', 'u']
# sentence = "Why am I such a badass who achieves what he wants?"
# counter = {
#            'a': 0,
#            'e': 0,
#            'i': 0,
#            'o': 0,
#            'u': 0
#           }

# for c in sentence:
#     if (c in vowels):
#         counter[c] += 1

sentence = "Why am I big boss? I mean really big boss."
counter = {}
# for c in sentence: 
#     if c in vowels:
#         try:
#             counter[c] += 1
#         except KeyError:
#             counter[c] = 1

# same thing, another implementation - without raising exceptions

# for c in sentence: 
#     if c in vowels:
#         if c in counter:
#             counter[c] += 1
#         else:
#             counter[c] = 1

# same thing, 3rd way of implementation
for c in sentence:
    if c in vowels:
        counter.setdefault(c, 0) # SETDEFAULT - someting useful while working with dicts 
        counter[c] += 1

print("#### END OF ANOTHER PROGRAM #### ")

print("For given sentence: {}, counter looks like:\n{}".format(sentence, counter))
print("\nIn more \"digestible\" form it looks like: ")
for kv in counter:
    print("{} occurs {} times".format(kv, counter[kv]))

print()
for kv in my_achievements:
    print(kv)

for kv in my_achievements:
    print(my_achievements[kv])