str = '0011011100000000000110100000000001010001'

# for i in range(0, 40, 8): 
for i in range(0, len(str), 8):
    print(str[i:i+8])
    print("which converts to: {}\n".format(int(str[i:i+8], 2)))
    

smaller_numbers = [1, 3, 5, 7]
bigger_numbers = [10, 30, 50, 70]

smaller_numbers.remove(7)
smaller_numbers.extend(bigger_numbers)
print(smaller_numbers)