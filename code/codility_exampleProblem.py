def solution(A, B, K):
    
    ret = 0
    for i in range(A, B + 1):
        if i % K == 0:
            ret += 1 

    return ret 

if __name__ == "__main__":
    print(solution(6, 11, 2))


# edge cases 
# optimization 