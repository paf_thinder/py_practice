#!/bin/python3 

def birthday(s, d, m):

    # s ; nrs on chocolate squares ; example input: 1 2 1 3 2 ; this is translated into a list 
    # d ; day of birth ; sum of m squares has to equal to it 
    # m ; month of birth ; for 3 months we take only 3 squares under consideration ; for m = 4 -> 4 squares ; for m = 2 -> 2 squares, etc 

    # len(s) # length of an array ; j - second iterator, has to be validated before use ; sum = sum of s's

    ret = 0 # ret = nr_of_possibilities 
    for i in range(len(s)):
        if i + m > len(s) + 1: # list index out of range 
            if sum(s[i:i+m-1]) == d:
                ret += 1
            continue

        if sum(s[i:i+m]) == d:
            ret += 1

    return ret 

# TODO - refactor ; # list index out of range case should not be checked at each iteration