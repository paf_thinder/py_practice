#!/bin/python3 

def duplicate_count(text):
    text = text.lower()
    ret = 0
    already_checked = []
    for i in range(len(text)):
        for j in range(i + 1, len(text)):
            if text[i] == text[j]:
                if text[i] in already_checked:
                    break
                print (f"{text[i]} == {text[j]}, i = {i}, j = {j}")
                ret += 1 
                already_checked.append(text[i])
                break 
                
    return ret 
                
if __name__ == "__main__":
    print (duplicate_count("abcdeaa"))