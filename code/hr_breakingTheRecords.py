#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'breakingRecords' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts INTEGER_ARRAY scores as parameter.
#

def breakingRecords(scores):
    highest_score = lowest_score = scores[0]
    ret = [0, 0] # ret[0] - breaking highest score ; ret[1] - breaking lowest score  

    iter = 1 
    # iterate through all elements of an array 
    for score in scores:
        print ("current score: {}\ncurrent highest score: {}\ncurrent lowest score: {}".format(score, highest_score, lowest_score))
        if score > highest_score:
            ret[0] += 1
            highest_score = score
            continue
        if score < lowest_score:
            ret[1] += 1
            lowest_score = score
            continue

        print ("After {} iteration: {}".format(iter, ret))
        iter += 1

    return ret 

if __name__ == '__main__':
    # fptr = open(os.environ['OUTPUT_PATH'], 'w')

    # n = int(input().strip())

    # scores = list(map(int, input().rstrip().split()))

    scores = [10, 5, 20, 20, 4, 5, 2, 25, 1]
    result = breakingRecords(scores)
    print (result)

    # fptr.write(' '.join(map(str, result)))
    # fptr.write('\n')

    # fptr.close()
