#!/bin/python3

'''

input: array of integers ar, positive integer k 
output: number of (i, j) pairs where i < j and ar[i] + ar[j] % k == 0 ; (ar[i] + ar[j] is divisible by k)

input: 
n, k - space separated integers ; n - len of ar array (== nr of elements in ar array)
arr - n space separated integers 

'''

def divisibleSumPairs(n, k, ar):
    ret = 0 
    for i in range(n):
        for j in range(i + 1, n):
            if (ar[i] + ar[j]) % k == 0:
                ret += 1

    return ret 

if __name__ == "__main__":
    print(divisibleSumPairs(6, 3, [1, 3, 2, 6, 1, 2])) 

# this took me literally less than 9 minutes 