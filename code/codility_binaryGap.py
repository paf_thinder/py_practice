# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def solution(N):
    # 32 -> 0 ; 100000 - no binary gaps
    # 1041 -> 5 ; 10000010001 - biggest binary gap is 5 
    iterate_over = bin(N)[2:]

    # if we have only 1 "1" in string iterated over ; return 0 - no binary gaps 
    if iterate_over.count("1") == 1 or iterate_over.count("0") == 0:
        return 0 

    splitted_bin = iterate_over.split("1")
    if splitted_bin[-1] == "0":
        # jeżeli ostatni element tablicy jest zerem, sprawdzamy czy w tablicy występuje chociaż raz taka sekwencja: "", "0", ""
        # jeżeli występuje - nie wiem co 
        # jeżeli nie występuje - zwróć 0 
        pass 

    ret = len(max(iterate_over.split("1")))

    return ret 


if __name__ == "__main__":
    print(solution(6))  # expected 0  # kiedy nie mamy 0 przedzielonego 1dynkami, nie mamy przerwy ; np. 110 - tu nie ma binary gap -> ['', '', 0]
    print(solution(328))  # expected 2 
    print(solution(51712))  # expected 2 
    print(solution(20))  # expected 1 