# steps 
# U - uphill step ; 1 Unit up
# D - downhill step ; 1 Unit down 

# mountain - starts with U, ends with D 
# valley - reversely

# iterate over each step 
# current_level = 0 
# if we go below 0 -> increment return (nr of valleys)
# in_valley = False 

in_valley = False 
current_level = 0
prev_level = current_level
ret = 0 # nr_of_valleys_visited

for step in steps:
    if step == "U":
        current_level += 1
    else:
        current_level -= 1 

    if current_level > 0:
        in_valley = False 

    if current_level < 0 and not in_valley:
        ret += 1 
        in_valley = True 

    if current_level == 0 and prev_level < 0:
        in_valley = False 

    # definitely required at the end of the loop
    prev_level = current_level

return ret 

#### 

def countingValleys(steps, path):
    in_valley = False 
    current_level = 0
    prev_level = current_level
    ret = 0 # nr_of_valleys_visited

    for step in steps:
        if step == "U":
            current_level += 1
        else:
            current_level -= 1 

        if current_level > 0:
            in_valley = False 

        if current_level < 0 and not in_valley:
            ret += 1 
            in_valley = True 

        if current_level == 0 and prev_level < 0:
            in_valley = False 

        # definitely required at the end of the loop
        prev_level = current_level

    return ret