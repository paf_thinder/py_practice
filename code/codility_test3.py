def solution(T):
    # write your code in Python 3.6
    length_of_season = int(len(T) / 4)
    winter = T[:length_of_season]
    spring = T[length_of_season : 2 * length_of_season]
    summer = T[2 * length_of_season : 3 * length_of_season]
    autumn = T[3 * length_of_season:]

    winter_amp = max(winter) - min(winter)
    spring_amp = max(spring) - min(spring)
    summer_amp = max(summer) - min(summer)
    autumn_amp = max(autumn) - min(autumn)

    amplitudes = [winter_amp, spring_amp, summer_amp, autumn_amp]
    highest_amp = max(amplitudes)

    if highest_amp == winter_amp:
        return "WINTER"
    elif highest_amp == spring_amp:
        return "SPRING"
    elif highest_amp == summer_amp:
        return "SUMMER"
    else:
        return "AUTUMN"

    # sry, tried to implement something more clever ; no success 
    #damplitudes = {"WINTER": winter_amp, "SPRING": spring_amp, "SUMMER": summer_amp, "AUTUMN": autumn_amp}
    #return str([key for key, value in amplitudes.items() if value == highest_amp])  # returns array of string ['AUTUMN']
    #return str({key for key, value in amplitudes.items() if value == highest_amp})  # returns dict of string {'AUTUMN'}

if __name__ == "__main__":
    T = [-3, -14, -5, 7, 8, 42, 8, 3]
    T2 = [2, -3, 3, 1, 10, 8, 2, 5, 13, -5, 3, -18]
    print(solution(T))
    print(solution(T2))
    # length_of_season = int(len(T) / 4)
    # # print(length_of_season)

    # winter = T[:length_of_season]
    # spring = T[length_of_season : 2 * length_of_season]
    # summer = T[2 * length_of_season : 3 * length_of_season]
    # autumn = T[3 * length_of_season:]

    # winter_amp = max(winter) - min(winter)
    # spring_amp = max(spring) - min(spring)
    # summer_amp = max(summer) - min(summer)
    # autumn_amp = max(autumn) - min(autumn)

    # amplitudes = [winter_amp, spring_amp, summer_amp, autumn_amp]

    # print(winter)
    # print(spring)
    # print(summer)
    # print(autumn)
    