def solution(A):
    current_node_value = A[0]
    nodes = 0
    while current_node_value != -1:
        nodes += 1 
        current_node_value = A[current_node_value]

    return nodes + 1 

    # A[0] == -1 ; checked 
    # every simple test case I could come up with is checked as well (:


if __name__ == "__main__":
    A = [1, 4, -1, 3, 2]
    B = [2, -1, 5, 77, 7, 1]
    C = [2, -1, 1, 27, 99, 1]
    D = [1, 3, 4, 2, 5, 9, 22, 33, 54, -1]

    #print(A[0])  # pierwszy węzeł - głowa ; wartość 1, indeks 0 
    #print(A[1])  # drugi węzeł         ; wartość 4, indeks 1 
    #print(A[4])  # trzeci węzeł        ; wartość 2, indeks 4 
    #print(A[2])  # czwarty węzeł       ; wartość -1, indeks 2 

    # print(A[0])
    # current_node_value = A[0]
    # print(A[current_node_value])
    # current_node_value = A[current_node_value]
    # print(A[current_node_value])
    # current_node_value = A[current_node_value]
    # print(A[current_node_value])

    print(solution(A))
    print(solution(B))
    print(solution(C))
    print(solution(D))

# TODO napisać kod, który sam będzie wymyślał tutaj przypadki testowe 
