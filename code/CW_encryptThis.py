import re 

''' 
# APPROACH 1 - mine 
def convertLetterToASCII(word):
    return str(ord(word[0])) + word[1:]

def switchSecondWithLast(word):
    # regex - find up to 3 digits at the beginning of the word 
    pattern = r"([0-9]{1,3})"
    regex = re.compile(pattern)
    match = regex.match(word) # matches pattern with word ; pattern is line above 
    temp_first_letter = match.group(1)
    truncated_from = len(temp_first_letter)
    word = word[truncated_from:]
    if len(word) == 1:
        return temp_first_letter + word
    try:
        second = word[0]
        last = word[-1]
    except: 
        return temp_first_letter

    return temp_first_letter + last + word[1:-1] + second

    # this approach won't work because we don't have char anymore at the 0 index, but its ASCII representation
    # return word[0] + word[-1] + word[2:-1] + word[1]

def encryptThis(text):
    text = text.split(" ")
    ret = []
    for word in text: 
        encrypted = switchSecondWithLast(convertLetterToASCII(word))
        ret.append(encrypted)
        
    return " ".join(ret)
''' 

# APPROACH 2 - super neat, and SO GOOD. Great practices ; I want to write code like this ; simple and elegant!!! 
def encryptThis(text):
    result = []
    for word in text.split(): # defaultly " "
        # turn word into a list? 
        word = list(word) # check if this works without this line ; no it does not ; string object does not support item assignement 
        # in -> ['i', 'n']
        # on -> ['o', 'n']
        # kakaowo -> ['k', 'a', 'k', 'a', 'o', 'w', 'o']

        word[0] = str(ord(word[0]))
        
        # switch 2nd and last letter 
        if len(word) > 2:
            word[1], word[-1] = word[-1], word[1] # clever swap 

        # see the differences? 
        # result.append(word) # when we work on list - list allows to change first char to number - you cannot use this kind of append - it appends list 
        # result.append(''.join(word)) # you have to use this kind of appends - it combines all the list elements back to the string 

    return " ".join(result)

# APPROACH 3 - codegolf 
    # requires swapper function
def swapper(w):
    return w if len(w) < 2 else w[-1] + w[1:-1] + w[0]

def cg_encryptThis(s):
    return ' '.join(w if not w else str(ord(s[0])) + swapper(w[1:]) for w in s.split()) # yeah, these are not best practices definitely ;]] 

if __name__ == "__main__":
    # print(convertLetterToASCII("wise")) # this works
    # print(switchSecondWithLast(convertLetterToASCII("wise"))) # this works 
    print (encryptThis("in an oak")) 