#! python3.9 

# create a dict 
# for each element of an array do arr.count(el)
# add el as a key ; arr.count(el) as a value 
# replace el with 0
# choose highest nr of value - skip value for 0 
# if there are multiple - choose the smaller one... 

# nie umiem kurwa w słowniki -.- 

#### 

def migratoryBirds(arr):
    # types of birds are 1, 2, 3, 4, 5 
    step_before_ret = []
    step_before_ret = [arr.count(1), arr.count(2), arr.count(3), arr.count(4), arr.count(5)]

    # naive method 
    # way too naive - fixed, learned, gg 
        # step_before_ret is okay 
        # find max in the step_before_ret 
        # find the index of this max with naive approach 
        # if index = 0 return 1 ; index = 1 return 2; index = 2 return 3 
            # return index + 1 
    maxval = max(step_before_ret)
    index = 0 
    ret = []

    # 2nd try - SUCCESSFUL! ; bravo. 
    for i in range(len(step_before_ret)):
        if step_before_ret[i] == maxval:
            index = i 
            break
    
    return index + 1 

    # TODO try different approach - enumeration and so on 
    # TODO check solutions of other people 

    '''FIRST APPROACH, UNSUCCESSFUL ; why do I learn and grow every single day?
    # what about the case when 1 is the most common nr - when nr of 1's should be returned? 
    # [5, 4, 3, 2, 1]
    for i in range(1, len(step_before_ret)):
        if step_before_ret[i] >= max:
            max = step_before_ret[i]
            ret.append(i)

    # if index 0 is the bigger, return '1' - according to the problem description
    if not ret:
        return 1 
    else:
        return min(ret)
    '''

# more 'pr0' approach
# use enumerate or filter 
# https://www.geeksforgeeks.org/python-ways-to-find-indices-of-value-in-list/ 

if __name__ == "__main__":
    inarr = [1, 4, 4, 4, 5, 3]

    print(migratoryBirds(inarr))