#!/bin/python3

def isLeap(year):
    if int(year) < 1918:
        if int(year) % 4 == 0:
            return True

        return False 

    else: 
        if int(year) % 400 == 0:
            return True 
        if int(year) % 4 == 0 and int(year) % 100 != 0:
            return True 
    
        return False 

def dayOfProgrammer(year):
    if year == 1918:
        # February has 28 days
        # January 31st converts into February 14th 
        # total nr of days in first 8 months are: 31 + 15 (!!! 14th February - 28th February == 15 days, lol) + 31 + 30 + 31 + 30 + 31 + 31 = 230
        # 256 - 230
        return f"26.09.{year}"

    if isLeap(year):
        # February has 29 days
        # total nr of days in first 8 months are: 31 + 29 + 31 + 30 + 31 + 30 + 31 + 31 = 244
        # 256 - 244 
        return f"12.09.{year}"

    else:
        # February has 28 days
        # total nr of days in first 8 months are: 31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 = 243
        # 256 - 243 
        return f"13.09.{year}"


if __name__ == "__main__":
    print(isLeap("2017"))
    print(isLeap("2016"))
    print(isLeap("1800"))
    print(isLeap("1918"))

    print(dayOfProgrammer("2000")) # expected: 12.09.2000 -> leap year 