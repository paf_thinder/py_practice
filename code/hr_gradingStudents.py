def gradingStudents(grades):
    for grade in grades:
        # edge case
        if grade < 38:
            print(grade)
            continue

        # find next multiple of 5 
        rem = grade % 5 # rem stands for remainder 
        if rem > 4: 
            print ("DIDNT SEE THAT COMING, WHAT HAPPENED?")
            break 
        
        closest_multiple = grade + (5 - rem)

        if closest_multiple - grade < 3:
            print (closest_multiple)
        else:
            print(grade) 

if __name__ == '__main__':
    #grades = [73, 67, 38, 33]
    grades = [37, 38]
    result = gradingStudents(grades)

#### 

''' 

#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'gradingStudents' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts INTEGER_ARRAY grades as parameter.
#

def gradingStudents(grades):
    ret = []
    for grade in grades:
        # edge case
        if grade < 38:
            #print(grade)
            ret.append(grade)
            continue

        # find next multiple of 5 
        rem = grade % 5 # rem stands for remainder 
        if rem > 4: 
            print ("DIDNT SEE THAT COMING, WHAT HAPPENED?")
            break 
        
        closest_multiple = grade + (5 - rem)

        if closest_multiple - grade < 3:
            #print (closest_multiple)
            ret.append(closest_multiple)
        else:
            #print(grade) 
            ret.append(grade)
            
    return ret 

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    grades_count = int(input().strip())

    grades = []

    for _ in range(grades_count):
        grades_item = int(input().strip())
        grades.append(grades_item)

    result = gradingStudents(grades)

    fptr.write('\n'.join(map(str, result)))
    fptr.write('\n')

    fptr.close()


'''