# thunderheads and cumulus 
# 1s and 0s - 0s are safe 

# you can jump on cumulus of number == number of current cloud + 1 or +2 
    # nr of current cloud = 1 ; u can jump on cumulus nr 2 or cumulus nr 3 
    # nr of cloud is its index in the input array 

# determine the minimum nr of jumps to reach the last cloud 

# SOLUTION:
# to achieve the goal in the minimum nr of moves, always TRY to move by clouds forward 
# example input: [0 0 1 0 0 1 0]

# simplest approach
# try to reach cloud +2 from you 
# if it's == 1 or it's beyond the scope of the list 
    # jump +1 from you 

# 2nd approach
# mark the positions of clouds to be avoided 

def jumpingOnClouds(c):
    lc = len(c) - 1 # last_cloud
    cc = 0 # current_cloud 
    ret = 0 

    while cc != lc:
        try:
            if c[cc + 2] == 1:
                cc += 1
                ret += 1
            else:
                cc += 2 
                ret += 1
        except:
            cc += 1
            ret += 1 

    return ret 

if __name__ == "__main__":
    print(jumpingOnClouds([0, 0, 1, 0, 0, 1, 0]))
    print(jumpingOnClouds([0, 0, 0, 0, 1, 0]))