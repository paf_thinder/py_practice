# Each calculation consist of exactly one operation and two numbers 
# e.g.: (seven(times(five())) # 3 closing brackets 
# The most outer function represents the left operand, the most inner function represents the right operand 
# integer division is required 

def plus(right_operand): 
    right_operand.append("+")
    return right_operand

def minus(right_operand): 
    right_operand.append("-")
    return right_operand

def times(right_operand): 
    right_operand.append("*")
    return right_operand

def divided_by(right_operand): 
    if right_operand == 0:
        # cannot divide by 0! 
        return 0 
    right_operand.append("/")
    return right_operand

def zero(opt_arg = 0):
    if opt_arg == 0:
        return [0]
    else:
        if opt_arg[1] == "*":
            return 0
        elif opt_arg[1] == "/":
            return 0 
        elif opt_arg[1] == "+":
            return opt_arg[0]
        elif opt_arg[1] == "-":
            return -opt_arg[0]


def one(opt_arg = 1):
    if opt_arg == 1:
        return [1]
    else:
        if opt_arg[1] == "*":
            return opt_arg[0]
        elif opt_arg[1] == "/":
            return 1 // opt_arg[0]
        elif opt_arg[1] == "+":
            return 1 + opt_arg[0]
        elif opt_arg[1] == "-":
            return 1 - opt_arg[0]

def two(opt_arg = 2):
    if opt_arg == 2:
        return [2]
    else:
        if opt_arg[1] == "*":
            return 2 * opt_arg[0]
        elif opt_arg[1] == "/":
            return 2 // opt_arg[0]
        elif opt_arg[1] == "+":
            return 2 + opt_arg[0]
        elif opt_arg[1] == "-":
            return 2 - opt_arg[0]

def three(opt_arg = 3):
    if opt_arg == 3:
        return [3]
    else:
        if opt_arg[1] == "*":
            return 3 * opt_arg[0]
        elif opt_arg[1] == "/":
            return 3 // opt_arg[0]
        elif opt_arg[1] == "+":
            return 3 + opt_arg[0]
        elif opt_arg[1] == "-":
            return 3 - opt_arg[0]

def four(opt_arg = 4):
    if opt_arg == 4:
        return [4]
    else:
        if opt_arg[1] == "*":
            return 4 * opt_arg[0]
        elif opt_arg[1] == "/":
            return 4 // opt_arg[0]
        elif opt_arg[1] == "+":
            return 4 + opt_arg[0]
        elif opt_arg[1] == "-":
            return 4 - opt_arg[0]

def five(opt_arg = 5):
    if opt_arg == 5:
        return [5]
    else:
        if opt_arg[1] == "*":
            return 5 * opt_arg[0]
        elif opt_arg[1] == "/":
            return 5 // opt_arg[0]
        elif opt_arg[1] == "+":
            return 5 + opt_arg[0]
        elif opt_arg[1] == "-":
            return 5 - opt_arg[0]

def six(opt_arg = 6):
    if opt_arg == 6:
        return [6]
    else:
        if opt_arg[1] == "*":
            return 6 * opt_arg[0]
        elif opt_arg[1] == "/":
            return 6 // opt_arg[0]
        elif opt_arg[1] == "+":
            return 6 + opt_arg[0]
        elif opt_arg[1] == "-":
            return 6 - opt_arg[0]

def seven(opt_arg = 7):
    if opt_arg == 7:
        return [7]
    else:
        if opt_arg[1] == "*":
            return 7 * opt_arg[0]
        elif opt_arg[1] == "/":
            return 7 // opt_arg[0]
        elif opt_arg[1] == "+":
            return 7 + opt_arg[0]
        elif opt_arg[1] == "-":
            return 7 - opt_arg[0]

def eight(opt_arg = 8):
    if opt_arg == 8:
        return [8]
    else:
        if opt_arg[1] == "*":
            return 8 * opt_arg[0]
        elif opt_arg[1] == "/":
            return 8 // opt_arg[0]
        elif opt_arg[1] == "+":
            return 8 + opt_arg[0]
        elif opt_arg[1] == "-":
            return 8 - opt_arg[0]

def nine(opt_arg = 9):
    if opt_arg == 9:
        return [9]
    else:
        if opt_arg[1] == "*":
            return 9 * opt_arg[0]
        elif opt_arg[1] == "/":
            return 9 // opt_arg[0]
        elif opt_arg[1] == "+":
            return 9 + opt_arg[0]
        elif opt_arg[1] == "-":
            return 9 - opt_arg[0]

# best solution from CW:
'''
def zero(f = None): return 0 if not f else f(0)
def one(f = None): return 1 if not f else f(1)
def two(f = None): return 2 if not f else f(2)
def three(f = None): return 3 if not f else f(3)
def four(f = None): return 4 if not f else f(4)
def five(f = None): return 5 if not f else f(5)
def six(f = None): return 6 if not f else f(6)
def seven(f = None): return 7 if not f else f(7)
def eight(f = None): return 8 if not f else f(8)
def nine(f = None): return 9 if not f else f(9)

def plus(y): return lambda x: x+y
def minus(y): return lambda x: x-y
def times(y): return lambda  x: x*y
def divided_by(y): return lambda  x: x/y
'''

if __name__ == "__main__":
    two(divided_by(six()))
    #print(six(divided_by(two())))
    print(six(divided_by(two())))
    #print(type(six(divided_by(two())))) # DONE, DONE, DONE! GG. 
    four(plus(nine()))
    eight(minus(three()))

# to było bardzo spoko 
# od momentu w którym totalnie nie wiedziałem co zrobić 
# przez odpalenie zepsutego kodu - błąd podpowiedział mi gdzie iść dalej 
# Brawo! super. dużo się tutaj nauczyłem! 

# jeszcze lepiej: 
    # WIĘCEJ PYTAŃ ZADAWAĆ SAMEMU SOBIE 
    # myśleć :) 

# TODO ; napisać kod, który będzie potrafił napisać taki kod ;]] 
# kod który będzie rozwiązywał problemy na podstawie examples 



# 1st trial - close, but not exactly 
''' 

def six(optional_arg = "6"): 
    # depending on the nr of arguments function behaves differently 
    if optional_arg == "6":
        return "6" 
    else: 
        return exec("(6{})".format(optional_arg))
        #print(type(exec("print(6{})".format(optional_arg)))) # TODO restart here ; jakbym tego printa mogl przekierowac do zmiennej ; stdout -> zmienna
        #print (type(ret))
        # return ret
        # return exec("print(6{})".format(optional_arg))


def divided_by(right_operand): 
    return "//"+right_operand
    #your code here

def two(optional_arg = "2"): 
    if optional_arg == "2":
        return "2"
    else:
        # print("2{}".format(optional_arg))
        return exec("print(2{})".format(optional_arg))

def zero(optional_arg = "0"): 
    if optional_arg == "0":
        return "0"
    else:
        return exec("print(0{})".format(optional_arg))

def one(optional_arg = "1"): 
    if optional_arg == "1":
        return "1"
    else:
        return exec("print(1{})".format(optional_arg))

def three(optional_arg = "3"): 
    if optional_arg == "3":
        return "3"
    else:
        return exec("print(3{})".format(optional_arg))

def four(optional_arg = "4"): 
    if optional_arg == "4":
        return "4"
    else:
        return exec("print(4{})".format(optional_arg))
        
def five(optional_arg = "5"): 
    if optional_arg == "5":
        return "5"
    else:
        return exec("print(5{})".format(optional_arg))

def seven(optional_arg = "7"): 
    if optional_arg == "7":
        return "7"
    else:
        return exec("print(7{})".format(optional_arg))

def eight(optional_arg = "8"): 
    if optional_arg == "8":
        return "8"
    else:
        return exec("print(8{})".format(optional_arg))

def nine(optional_arg = "9"): 
    if optional_arg == "9":
        return "9"
    else:
        return exec("print(9{})".format(optional_arg)) 
''' 