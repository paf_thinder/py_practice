import sys, random, argparse
import numpy as np 
import math
import turtle
import random 

from PIL import Image
# from Pillow import Image
from datetime import datetime
from fractions import gcd 

# class which draws spirograph 
class Spiro:
    def __init__(self, xc, yc, col, R, r, l):

        self.t = turtle.Turtle()
        # set the shape of the cursor
        self.t.shape('turtle')
        # set the step [degrees]
        self.step = 5
        self.drawingComplete = False

        self.setparams(xc, yc, col, R, r, l)
        self.restart()

    def setparams(self, xc, yc, col, R, r, l):
        self.xc = xc
        self.yc = yc
        self.col = col
        self.R = int(R)
        self.r = int(r) 
        self.l = l 

        # zredukowanie r/R do najmniejszej postaci poprzez podzielenie przez NWD 
        gcdVal = gcd(self.r, self.R) # gcd = greatest common divisor
        self.nRot = self.r / gcdVal

        # uzyskanie stosunku promieni 
        self.k = r / float(R)

        # ustawienie koloru 
        self.t.color(*col) # *col - wtf? wskaźnik? lol 

        # zapisanie bieżącego kąta
        self.a = 0

    def restart(self):
        self.drawingComplete = False
        self.t.showturtle()

        # przejście do pierwszego punktu
        self.t.up()
        R, k, l = self.R, self.k, self.l 
        a = 0.0 # float
        x = R * ((1 - k) * math.cos(a) + l * k * math.cos((1 - k) * a / k))
        y = R * ((1 - k) * math.sin(a) - l * k * math.sin((1 - k) * a / k))
        self.t.setpos(self.xc + x, self.yc + y)
        self.t.down()

    # rysowanie całości 
    def draw(self):
        # rysowanie reszty punktów
        R, k, l = self.R, self.k, self.l 
        for i in range(0, 360 * self.nRot + 1, self.step):
            a = math.radians(i)
            x = R * ((1 - k) * math.cos(a) + l * k * math.cos((1 - k) * a / k))
            y = R * ((1 - k) * math.sin(a) - l * k * math.sin((1 - k) * a / k))
            self.t.setpos(self.xc + x, self.yc + y)

        # rysowanie ukończone, ukrywamy żółwia, lol ;] 
        self.t.hideturtle()

    def update(self):
        # pominięcie reszty kroków jeżeli zrobione
        if self.drawingComplete:
            return 

        # inkrementowanie kąta 
        self.a += self.step

        # rysowanie kroku 
        R, k, l = self.R, self.k, self.l 

        # ustawianie kąta
        a = math.radians(self.a)
        x = self.R * ((1 - k) * math.cos(a) + l * k * math.cos((1 - k) * a / k))
        y = self.R * ((1 - k) * math.sin(a) - l * k * math.sin((1 - k) * a / k))
        self.t.setpos(self.xc + x, self.yc + y)

        # jeśli rysowanie ukończone - ustawianie flagi
        if self.a >= 360 * self.nRot:
            self.drawingComplete = True

            # rysowanie ukończone, ukrywamy żółwia ;] ;d 
            self.t.hideturtle()

    def clear(self):
        self.t.clear()

class SpiroAnimator:

    def __init__(self, N):
        # ustawienie wartości licznika w milisekundach
        self.deltaT = 10

        # uzyskanie rozmiarów okna
        self.width = turtle.window_width()
        self.height = turtle.window_height()

        # tworzenie obiektów spiro
        self.spiros = []
        for i in range(N):
            # generowanie losowych parametrów
            rparams = self.genRandomParams()
            # ustawianie parametrów spiro 
            spiro = Spiro(*rparams) # TODO *again - what is that? 
            self.spiros.append(spiro)

        # wywołanie timera
        turtle.ontimer(self.update, self.deltaT)

    def restart(self):
        for spiro in self.spiros:
            spiro.clear()
            # generowanie losowych parametrów 
            rparams = self.genRandomParams()
            # ustawianie parametrów spiro
            spiro.setparams(*rparams)
            # zrestartowanie rysowania
            spiro.restart()

    def genRandomParams(self):
        width, height = self.width, self.height
        R = random.randint(50, min(width, height) // 2)
        r = random.randint(10, 9 * R // 10)
        l = random.uniform(0.1, 0.9)
        xc = random.randint(-width // 2, width // 2)
        yc = random.randint(-height // 2, height // 2)
        col = (random.random(),
               random.random(),
               random.random())
        return (xc, yc, col, R, r, l)

    def update(self):
        # aktualizowanie wszystkich krzywych spiro
        nComplete = 0 
        for spiro in self.spiros:
            spiro.update()
            # zliczanie ukończonych krzywych 
            if spiro.drawingComplete:
                nComplete += 1
            
        # jeśli wszystkie krzywe spiro są ukończone, restartowanie
        if nComplete == len(self.spiros):
            self.restart()

        # wywołanie timera
        turtle.ontimer(self.update, self.deltaT)

    # włączanie i wyłączanie kursora żółwia
    def toggleTurtles(self):
        for spiro in self.spiros:
            if spiro.t.isvisible():
                spiro.t.hideturtle()
            else:
                spiro.t.showturtle()

def saveDrawing():
        turtle.hideturtle()
        # generowanie unikatowej nazwy pliku 
        dateStr = (datetime.now().strftime("%d%b%Y-%H%M%S"))
        fileName = 'spiro-' + dateStr
        print("Zapisywanie rysunku w pliku %s.eps/png" % fileName)
        # uzyskanie obiektu canvas modułu tkinter
        canvas = turtle.getcanvas()
        # zapisywanie rysunku w obrazie postscript 
        canvas.postscript(file = fileName + '.eps')
        # użycie modułu Pillow do konwersji pliku obrazu postscript na PNG 
        img = Image.open(fileName + '.eps')
        img.save(fileName + '.png', 'png')
        turtle.showturtle()


def main():
    # użycie sys.argv w razie potrzeby 
    print("Generowanie spirografu... ")
    # tworzenie parsera 
    descStr = """
                Ten program rysuje krzywe spiro, używając modułu turtle.
                Program uruchomiony bez żadnych argumentów rysuje losowe krzywe spiro. 

                Terminologia:
                R - promień zewnętrznego okręgu
                r - promień wewnętrznego okręgu 
                l - stosunek odcinka poprowadzonego ze środka mniejszego okręgu do punktu umieszczenia końcówki pióra do promienia r 
              """

    parser = argparse.ArgumentParser(description=descStr)

    # dodanie oczekiwanych argumentów 
    parser.add_argument('--sparams', nargs = 3, dest = 'sparams', required = False, 
                        help = "Trzy argumenty w sparams: R, r, l.")

    # parsowanie args
    args = parser.parse_args()

    # ustawienie szerokości rysowania na 80% szerokości obrazu 
    turtle.setup(width = 0.8)

    turtle.shape('turtle')
    turtle.title('Spirografy!')

    # dodanie procedury obsługi przycisku dla zapisywania rysunków 
    turtle.onkey(saveDrawing, "s")
    # rozpoczęcie nasłuchiwania 
    turtle.listen()

    turtle.hideturtle()

    # sprawdzenie argumentów wysłanych do --sparams i rysowanie spirografu 
    if args.sparams:
        params = [float(x) for x in args.sparams]
        # rysowanie spiro z danymi parametrami 
        col = (0.0, 0.0, 0.0)
        spiro = Spiro(0, 0, col, *params) # TODO * params? 
        spiro.draw()
    else:
        # tworzenie obiektu animatora
        spiroAnim = SpiroAnimator(4) # TODO dlaczego 4? 
        # dodanie procedury obsługi przycisku dla włączania i wyłączania kursora żółwia 
        turtle.onkey(spiroAnim.toggleTurtles, "t")
        # dodanie procedury obsługi dla restartowania animacji 
        turtle.onkey(spiroAnim.restart, "space")

    turtle.mainloop()

if __name__ == '__main__':
    main() 