"""
test.py

Testowanie grafiki Turtle

Autor: Mahesh Venkitachalam
Strona WWW: electronut.in
"""

import math
import turtle

# rysowanie okręgu za pomocą turtle
def drawCircleTurtle(x, y, r):
    # got to start of circle
    turtle.up()
    turtle.setpos(x + r, y)
    turtle.down()


    # rysowanie okręgu
    for i in range(0, 365, 5):
        a = math.radians(i)
        turtle.setpos(x + r*math.cos(a), y + r*math.sin(a))

# rysowanie spirali za pomocą turtle
def drawSpiralTurtle(x, y, r):
    # przejście do początku spirali
    turtle.up()
    turtle.setpos(x + r, y)
    turtle.down()

    # rysowanie spirali
    for i in range(0, 360*10, 5):
        a = math.radians(i)
        x = r*math.cos(a)*math.exp(0.05*a)
        y = r*math.sin(a)*math.exp(0.05*a)
        turtle.setpos(x, y)

def main():
    print('testowanie...')
    
    #drawCircleTurtle(100, 100, 50)

    drawSpiralTurtle(0, 0, 5)

    turtle.mainloop()

# wywołanie main
if __name__ == '__main__':
  main()
