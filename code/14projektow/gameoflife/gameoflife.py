# warum this shit funktioniert nicht?!?!?! #! python 3.8

# Conway's game of life ;] lol 

# 1. initialize cells on the field 
# 2. according to the timestamp of the simulation perform certain action for each cell: 
    # - update cell (i, j) value according to the state of its neighbours according to the edge cases (? z uwzględnieniem warunków brzegowych)
    # - update display  

import numpy as np 
import argparse
import matplotlib.pyplot as plt # FOK no matplotlib installed -.- 
import matplotlib.animation as animation 

# numpy
# matplotlib
# image 
# pillow 
# TODO make it work 

OFF = 0 
vals = [ON, OFF]

def randomGrid(N):
    """return random grid of NxN size"""
    return np.random.choice(vals, N * N, p=[0.2, 0.8]).reshape(N, N)

def addGlider(i, j, grid):
    '''adds glider with left upper cell in (i, j)'''
    glider = np.array([[0, 0, 255],
                      [255, 0, 255],
                      [0, 255, 255]])
    grid[i:i+3, j:j+3] = glider 
    print(grid[i:i+3, j:j+3])

def update(frameNum, img, grid, N):
    # copy the grid, because we need 8 neighbours for calculation 
    newGrid = grid.copy()
    for i in range(N):
        for j in range(N):
            # calculate the sum for 8 neighbours using toroidal boundary conditions 
            # x and y are 'rolled' (zawijane), so the simulation occurs in the 
            # toroidal space 
            total = int((grid[i, (j - 1) % N] + grid[i, (j + 1) % N] +
                         grid[(i - 1) % N, j] + grid[(i + 1) % N, j] +
                         grid[(i - 1) % N, (j - 1) % N] + grid[(i + 1) % N, (j - 1) % N] + 
                         grid[(i + 1) % N, (j - 1) % N] + grid[(i + 1) % N, (j + 1) % N]) / 255) # TODO why / 255?  

            # applying Conway's rules
            if grid[i, j] == ON:
                if (total < 2) or (total > 3):
                    newGrid[i, j] = OFF
            else:
                if total == 3:
                    newGrid[i, j] = ON

        # update the data
    img.set_data(newGrid)
    grid[:] = newGrid[:] # TODO ; what does that mean? [:]
    return img, # TODO ; return + , at the end? 

    # comments to this function
    # copy the grid, because we need 8 neighbours for calculation 
    # TODO - what does the upper line mean? 
        # why do we need 8 neighbours? ; 8 neighbours, because thats how many neighbours each cell has 

            #

                # [0, 0, 0
                #  0, 0, 0
                #  0, 0, 0]

            #

        # how do we get 8 neighbours? ; calculating total i - 1 <- one row higher ; j + 1 <- one column to the right 
        # and so on and so forth 

    # then traverse line by line


def main():
    # arguments of cmd line are 'located' in sys.argv[1], sys.argv[2], ... 
    # sys.argv[0] is the name of the script and thus can be ignored

    # parse the arguments 
    parser = argparse.ArgumentParser(description = "Starting Comway's game of life")

    # add the arguments
    parser.add_argument('--grid-size', dest='N', required=False)
    parser.add_argument('--mov-file', dest='movfile', required=False) 
    parser.add_argument('--interval', dest='interval', required=False)
    parser.add_argument('--glider', action='store_true', required=False)
    parser.add_argument('--gosper', action='store_true', required=False)
    args = parser.parse_args() # apparently no need to def parse_args() ; of course not, Simon! don't be silly! 

    # set the size of the grid 
    N = 100 
    if args.N and int(args.N) > 8:
        N = int(args.N) # TODO - what does it do? 

    # set the interval
    updateInterval = 50 
    if args.interval:
        updateInterval = int(args.interval)

    # declare the grid 
    grid = np.array([])

    # check if flag 'glider' was set up 
    if args.glider:
        grid = np.zeros(N * N).reshape(N, N)
        addGlider(1, 1, grid)
    elif args.gosper:
        grid = np.zeros(N * N).reshape(N, N)
        addGosperGliderGun(10, 10, grid) # TODO this function not mentioned ever before ; implement ;] 
    else:
        # fill the grid with randomly generated cells ; more OFF then ON cells (probability p)
        grid = randomGrid(N)

    fig, ax = plt.subplots()
    img = ax.imshow(grid, interpolation = "nearest")
    ani = animation.FuncAnimation(fig, update, fargs=(img, grid, N, ), 
                                  frames = 10,
                                  interval = updateInterval,
                                  save_count = 50
                                 )
    # nr of frames? ; lol they even don't know in the book what is this line ; p. 71                             
    # configure output file
    if args.movfile:
        ani.save(args.movfile, fps=30, extra_args=['-vcodec', 'libx264'])

    plt.show()

if __name__ == "__main__":
    main()

# TODO 
# https://stackoverflow.com/questions/8863917/importerror-no-module-named-pil 
# import Image - installed ; but 'no module named Image' 
# import Pillow - installed 

# https://translate.google.com/?sl=auto&tl=ru&text=ucz%C4%99%20si%C4%99%2C%20%C5%BCeby%20dosta%C4%87%20lepsz%C4%85%20prac%C4%99.%20Przynajmniej%20godzin%C4%99%20dziennie%20%0A%0Adzi%C4%99kuj%C4%99%20bardzo%2C%20pyszna%20herbata&op=translate

# https://translate.google.com/?sl=auto&tl=es&text=ucz%C4%99+si%C4%99,+%C5%BCeby+dosta%C4%87+lepsz%C4%85+prac%C4%99.+Przynajmniej+godzin%C4%99+dziennie+&op=translate