def persistence(n):
    result_of_temporary_multiplication = 1
    nrs_of_multiplications = 0 
    
    if n < 10:
        return 0 
    
    while True:
        # create string out of n ; so I can access every single digit 
        for digit in str(n):
            result_of_temporary_multiplication *= int(digit)
        
        nrs_of_multiplications += 1

        if len(str(result_of_temporary_multiplication)) == 1: 
            break

        n = result_of_temporary_multiplication 
        result_of_temporary_multiplication = 1
        
    return nrs_of_multiplications 

if __name__ == "__main__":
    print(persistence(39))