#!/bin/python3

def pageCount(n, p):
    # edge cases 
    if n == p or p == 1:
        return 0 

    ret = []

    # starting from the front 
    ret.append(int(p/2))


    # starting from the back
    # check if the nr of pages is even or odd 
    if n % 2 == 0:
        if p % 2 != 0:
            p -= 1 

        ret.append((n-p)/2)

    else:
        # third edge case 
        if p == n - 1:
            return 0 

        if p % 2 == 0:
            p += 1 

        ret.append((n-p)/2)

    # return min of the array 
    return int(min(ret))


if __name__ == "__main__":
    print (pageCount(7, 4))
    print (pageCount(6, 5))



'''

def pageCount_rozkminka(n, p):
    if n == p or p == 1:
        return 0 

    ret = []
    # starting from the front 
    # 1 
    # 2 3 ; ret = 1 
    # 4 5 ; ret = 2 
    # 6 7 ; ret = 3 
    # 8 9 ; ret = 4 
    # 10 11 ; ret = 5 
    # 12 13 
    # 14 15 
    # 16 17 

    ret.append(int(p/2))


    
    # starting from the back
    # check if the nr of pages is even or odd 
    if n % 2 == 0:
        # n 
        # n-1, n-2 
        # n-3, n-4

        # n = 10 
        # 10 ; ret = 0
        # 8 9 ; ret = 1 
        # 6 7 ; ret = 2 
        # 4 5 ; ret = 3 
        # 2 3 ; ret = 4 
        # 1 ; ret = 5 

        # n = 8 
        # 8 ; ret = 0
        # 6 7 ; ret = 1 
        # 4 5 ; ret = 2 
        # 2 3 ; ret = 3 

        if p % 2 != 0:
            p -= 1 # kierwa, seems okay 

        ret.append((n-p)/2)

    else:
        # n - 1, n 
        # n - 3, n - 2 
        # n - 5, n - 4
        # n - 7, n - 6
        if p == n - 1:
            return 0 
        
        # n = 13 
        # 12 13 ; ret 0
        # 10 11 ; ret 1 
        # 8 9 ; ret 2 
        # 6 7 ; ret 3 
        # 4 5 ; ret 4
        # 2 3 ; ret 5
        # 1 ; ret 6 
        if p % 2 == 0:
            p += 1 

        ret.append((n-p)/2)


    # return min of the array 
    return min(ret)

'''