#!/bin/python3 

def well(x):
    c = x.count('good')
    return 'I smell a series!' if c > 2 else 'Publish!' if c else 'Fail!'

# well of ideas 

### 

def well(x):
    good_counter = 0
    for el in x:
        if el == "good":
            good_counter += 1
    #return "Publish!" if good_counter == 1 else return "I smell a series!" if good_counter >= 2 else return "bad"
    
    if good_counter in [1, 2]:
        return "Publish!"
    elif good_counter > 2: 
        return "I smell a series!"
    else: 
        return "Fail!"

### 

'''

the difference between the two? 
- in my solution I use return, return, return
- in his there is no repetition in returns ; return statement can occur only once! 

'''