def solution(N):
    # edge_input = [x for x in range(999999990, 999999999 + 1)]
    # not greater == less or equal, therefore return for edge_input above is also correct 

    return N + (10 - N % 10)

if __name__ == "__main__":
    # "ordinary cases"
    #print(solution(27))
    #print(solution(34))
    #print(solution(55))

    # edge case 0 
    # print(solution(0)) # no need 
    #print(solution(1)) # good
    #print(solution(2)) # good
    
    
    # print(solution(999999999)) # 1000000000 - this solution is unacceptable 

    print(solution(999999989)) # 999999990 - good 

    print(solution(999999990)) # not good 

    
    print(bad_input)




    