#!/bin/python3

def two_sort(array):
    array = sorted(array)
    ret = ""
    for el in sorted(array)[0]:
        el += "***"
        ret += el 

    return ret[:-3]

if __name__ == "__main__":
    arr = ["turns", "out", "random", "test", "cases", "are", "easier", "than", "writing", "out", "basic", "ones"]
    print(two_sort(["turns", "out", "random", "test", "cases", "are", "easier", "than", "writing", "out", "basic", "ones"]))



