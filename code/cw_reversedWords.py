#!/bin/python3

#8kyu 

def reverse_words(s):
    ret = ""
    for i in range(len(s.split(" ")) - 1, -1, -1):
        ret += s.split(" ")[i] + " "
    
    return ret[:-1]

if __name__ == '__main__':
    # s = "The greatest victory is that which requires no battle"
    # print (sinput.split(" "))
    s = "hello world!"

    #for i in range(len(s.split(" ")) - 1, -1, -1):
    #    print (s.split(" ")[i])

    print (reverse_words(s))