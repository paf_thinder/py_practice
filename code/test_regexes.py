# regex in a browser
# write 3 tests for data in the browser 
# if works - finish encryptThis 

import unittest
import re

PATTERN = r"([0-9]{1,3})"
regex = re.compile(PATTERN)

# match = regex.match("119esi")
# print (match)
# print (match.group(1))
# print (type(match.group(1)))

class TestRegex(unittest.TestCase):

    RE_PATTERN = PATTERN

    def setUp(self):
        self.regex = re.compile(self.RE_PATTERN)

    def assertMatchString(self, string, match_string):
        match = self.regex.match(string)
        self.assertEqual(match.group(1), match_string)

    def assertDoesntMatchString(self, string, match_string):
        match = self.regex.match(string)
        self.assertNotEqual(match.group(1), match_string)

    # THIS IS TEST IT WILL BE EXECUTED
    def test_3digits(self):
        string = r"119esi"
        self.assertMatchString(string, "119")

    # THIS IS TEST IT WILL BE EXECUTED
    def test_3digits_redundant(self):
        string = r"123cmon"
        self.assertDoesntMatchString(string, "xyz")

    def test_2digits(self):
        string = r"12xyz"
        self.assertMatchString(string, "12")

        string = r"77xfff"
        self.assertMatchString(string, "77")
        
        string = r"85ghtyertytrhfgcbgfhdfgh43543"
        self.assertMatchString(string, "85")

        # okay, it works, this test is executed as well 
        # those lines ; because test is only 'def' here
        # so all these lines within the def test_2digits count as 
        # a single test 
        
        #string = r"44ghtyertytrhfgcbgfhdfgh43543"
        #self.assertMatchString(string, "85")
    
if __name__ == "__main__":
    unittest.main()