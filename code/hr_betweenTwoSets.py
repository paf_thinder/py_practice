#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'getTotalX' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER_ARRAY a
#  2. INTEGER_ARRAY b
#

def getTotalX(a, b): 
    iter = 0 

    for i in range(1, max(b) + 1):
        cond1 = True # condition 1 - TODO description 
        cond2 = True # condition 2 - TODO description

        for el in a:
            if i % el != 0:
                cond1 = False
                break 

        for el in b:
            if el % i != 0:
                cond2 = False
                break
        
        if cond1 and cond2:
            iter += 1
            print ("{} fulfills both conditions".format(i))

        # debugging purpose

    return iter



if __name__ == '__main__':
    a = [1]
    b = [100]
    print(getTotalX(a, b))
    # fptr = open(os.environ['OUTPUT_PATH'], 'w')

    # first_multiple_input = input().rstrip().split()

    # n = int(first_multiple_input[0])

    # m = int(first_multiple_input[1])

    # arr = list(map(int, input().rstrip().split()))

    # brr = list(map(int, input().rstrip().split()))

    # total = getTotalX(arr, brr)

    # fptr.write(str(total) + '\n')

    # fptr.close()
