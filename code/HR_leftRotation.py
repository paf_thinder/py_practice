def rotLeft(a, d):
    list = [x + 1 for x in range(a)] # list comprehension madafaka! 
    newlist = [0 for x in range(a)]

    for i in range(a):
        current_index = i
        new_index = (current_index - d) % a
        newlist[new_index] = list[i]

    return newlist

if __name__ == "__main__":
    print(rotLeft(5, 4))
    print(rotLeft(8, 12))

    # [1, 2, 3, 4, 5]

    # d = 1 
    # [2, 3, 4, 5, 1] 

# current_index - d 
# 0 - 1 = 4 -> -1 == 4 
# 0 - 1 = 1%4 

# d = 4 
# dla 1: 
# 0 - 4 = -4 ; [0, 1, 0, 0, 0]

# 2: 
# 1 - 4 = -3 ; [0, 1, 2, 0, 0]

# 3: 
# 2 - 4 = -2 ; [0, 1, 2, 3, 0]

# a = 5 ; d = 4 
# [5, 1, 2, 3, 4]

# start 
# a = 8 ; d = 12 
# [1, 2, 3, 4, 5, 6, 7, 8]
# [0, 0, 0, 0, 0, 0, 0, 0] 

# new_index = (current_index - d) % a