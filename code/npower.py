def power(n):
    if (n == 1):
        return 1
    else:
        return (n * power(n-1))

    # power works properly, no surprises
    #   if (n < 1): this kind of condition would do the job as well 
    #   in case interpreter misses int, or sth like that 

def fibonaccis(n):
    if (n == 0):
        return 0
    if (n == 1):
        return 1 
    else:
        return (fibonaccis(n-1) + fibonaccis(n-2))  

def tribonacci(signature, n):
    if (n == 0):
        return []
    
    ret = signature 
    
    if (n < 3):
        return ret[:n]
    
    l = 3

    #while(l < n-1):
    #    l = len(ret)
    #    ret.append(ret[l - 1] + ret[l - 2] + ret[l - 3])

        # vs 

    while(l < n):
        ret.append(ret[l - 1] + ret[l - 2] + ret[l - 3])
        l = len(ret) # MUCHO GUSTO! 
        # subtle difference, but does the job 
        
    return ret 

def tribonacci_refactored(beginning, nrofels):
    if (nrofels == 0):
        return []
    ret = beginning 
    if (nrofels < 3):
        return ret[:nrofels]
    l = 3
    while(l < nrofels):
        ret.append(ret[l-1] + ret[l-2] + ret[l-3])
        l = len(ret)
    return ret

    # I AM NOT TIGER WOODS OF CODE GOLF YET, BUT I AM IN PROGRESS! 
    # I am already awesome, loving this life, working on being even more awesome! ;] 

if __name__ == "__main__":
    # here the main program logic will be placed 
    # print("will it work?") # yes, it will, mate 

    # print(power(3))
    # print(power(6))

    # print(fibonaccis(8))
    print(tribonacci([170, 94, 195], 4))