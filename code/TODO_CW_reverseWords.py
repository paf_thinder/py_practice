def reverse_words(text):
    ret = []
    nr_of_spaces = 0
    is_space = False
    
    # counting the number of spaces between the words 
    for i in range(len(text)):
        if nr_of_spaces > 0 and not is_space:
            break

        if text[i] == " ":
            nr_of_spaces += 1
            is_space = True 
        else:
            is_space = False 
    for word in text.split():
        # fuck_yet_another_variable = word[::-1]
        ret.append(word[::-1])
    # print(ret)
        
    # return " ".join(ret)
    return "{}".format(nr_of_spaces * " ").join(ret)

# simpler, more elegant way
def reverse_words_pr0(str):
    return ' '.join(s[::-1] for s in str.split(' '))
    # TODO ; how. how? ; explain 

if __name__ == "__main__":
    print(reverse_words('The quick brown fox jumps over the lazy dog.'))
    print(reverse_words("double  spaced  words")) # how to check the number of spaces? 
    print(reverse_words_pr0('The quick brown fox jumps over the lazy dog.'))
    print(reverse_words_pr0("double  spaced  words"))
