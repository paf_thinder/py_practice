# all language elements are in this code 

a = 4 
b = 5 
c = [6, 7]

d = "Hi"
"Hello"

def area(x):
    return x**2

area(3)

'''
objects in this code are: 
4, 5, [6, 7] - 2 el.'s list ; 6, 7 - elements of this list ; "Hi" - assigned string ; "Hello" - unassigned string
function definition, 2, 3
'''

'''
identifiers (aka variables ; identifier is more documentation-like name) in this code are:
a, b, c, d, area, x 
'''

'''
operators: **
'''

'''
delimiters: = ; , ; "" ; ( ) ; : ; [] <- everything separated with ; let's make it more readable
delimiters: =   ,   ""   ( )   :   [] <- these are delimiters in python 
'''

'''
keywords: def, return
'''

'''
other language elements:
comments, 
blank lines,
white spaces,
indentation
'''

#### 

# sequential data types: strings, tuples, lists ; sequential == order DOES matter 
# 
# unordered data type example: SET 
    # set is unordered collection of items
    # in set every item is unique (no duplicates allowed)
    # in set every item is immutable (cannot be updated)
    # sets can store items of different types