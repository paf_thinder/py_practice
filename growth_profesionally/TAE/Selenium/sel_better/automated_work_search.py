# 2. open all offers in the following tabs
# <a class="jss2310 jss*"> ; klikaj w tego hrefa 

# 3. check the level 
# <div class="css-1ji7bvd">senior</div>

# maybe that's enough for now 

# TODO 
# 1st - "manually", without POM 
# 2nd - refactor with POM 

# this tut seems like very useful 
# https://www.youtube.com/watch?v=y0u_xeSscyM&list=PLUDwpEzHYYLvx6SuogA7Zhb_hZl3sln66&index=11

# freecodecamp - should be pure class, let's see
# https://www.youtube.com/watch?v=j7VZsCCnptM

# TODO - restart here 
# extract links is kinda working 
# but it extracts EVERYTHING 
# and I need to extract only certain portions 
# ANOTHER QUALIFIER IS REQUIRED 
# additional qualifier 
# something to narrow the results of find_elements search results 

# TODO - lol, apply: 
# https://justjoin.it/offers/mobica-test-engineer-krakow

from selenium import webdriver
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.common.by import By 
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC 
import time 
import re

PATH = "C:\Program Files (x86)\chromedriver.exe"
co_instance = webdriver.ChromeOptions()
co_instance.binary_location = r"C:\Program Files\BraveSoftware\Brave-Browser\Application\brave.exe"
driver = webdriver.Chrome(PATH, options=co_instance)

driver.get("https://justjoin.it/?q=Test%20Automation@skill;Krak%C3%B3w@city")
time.sleep(.1)
driver.maximize_window()
time.sleep(.1)

# tutaj chciałbym znaleźć class name zawierający na początku stringa to 
pattern = 'jss2310'

# to się za każdym razem inaczej nazywa!!! 
# work_offer = driver.find_element_by_class_name("jss229 jss512")
# work_offer = driver.find_element_by_partial_link_text("/offers/")	
work_offers = driver.find_elements(By.TAG_NAME, "a")
print(work_offers)
for link in work_offers:
    print(link.text)


try:
        work_offer_link = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME, "jss2310 jss*"))
        )
        #belly = driver.find_element_by_class_name("pajacyk__clickbox")
        work_offer_link.click()
        time.sleep(randint(1, 2))

except: 
        print("Things went wrong, check the log, pr0")
        driver.quit()

time.sleep(2)