# rls stands for real life scenario ; lol 
# got it from: 
    # https://stackoverflow.com/questions/68636955/how-to-long-press-press-and-hold-mouse-left-key-using-only-selenium-in-python 

import os 
from selenium import webdriver
from selenium.webdriver import ActionChains 
from selenium.webdriver.common.action_chains import ActionChains 

from time import sleep # can use sleep(5) instead of time.sleep(5)
from random import randint 

chromedriver_path = r"C:\Program Files (x86)\chromedriver.exe"
iChromeOptions = webdriver.ChromeOptions()
iChromeOptions.binary_location = "C:\Program Files\BraveSoftware\Brave-Browser\Application\brave.exe"
# os.environ["webdriver.chrome.driver"] = chromedriver # jic
driver = webdriver.Chrome(chromedriver_path, options=iChromeOptions)

url = "{PLACEHOLDER}" # if to be tested ; TODO choose the webpage 
driver.get(url)
sleep(randint(2, 3))

element = driver.find_element_by_xpath("//div[@id='px-captcha']")
# print(len(element.text), '-Value found by method text')
action = ActionChains(driver)
click = ActionChains(driver)
frame_x = element.location['x']
frame_y = element.location['y']
# print('x: ', frame_x)
# print('y: ', frame_y)
# print('size box: ', element.size)
# print('x max click: ', frame_x + element.size['width'])
# print('y max click: ', frame_y + element.size['height'])
x_move = frame_x + element.size['width'] * 0.5
y_move = frame_y + element.size['height'] * .5 

action.move_to_element_with_offset(element, x_move, y_move).click_and_hold().perform()
sleep(10)
action.release(element)
action.perform()
sleep(.2)

action.release(element)
