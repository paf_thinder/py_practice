from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time 

# this part of code is required to run the browser ; at least in my case 
PATH = r"C:\Program Files (x86)\chromedriver.exe"
iChromeOptions = webdriver.ChromeOptions()
iChromeOptions.binary_location = r"C:\Program Files\BraveSoftware\Brave-Browser\Application\brave.exe"
driver = webdriver.Chrome(PATH, options=iChromeOptions)

# when Chrome is installed this will be enough 
#PATH = r"C:\Program Files (x86)\chromedriver.exe"
#driver = webdriver.Chrome(PATH)

'''
    # some artificial data
    user_name = "grzegorz"
    password = "donotlookhere"

    # now manipulate the browser automatically 

    # open particular URL 
    driver.get("https://www.facebook.com") 

    # find element to interact with by id - best practice ; id's are unique
    element = driver.find_element_by_id("email")  

    # send variable # automatically type it into the field
    element.send_keys(user_name) 

    # find the field where you type the password in 
    element = driver.find_element_by_id("pass") 
    element.send_keys(password)

    # confirm logging in by pressing ENTER
    element.send_keys(Keys.RETURN) 
    # element.close() # WebElement has no attribute close 
'''

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait 
import time

# browser = webdriver.Firefox() 
browser = driver
browser.get("https://facebook.com")
username     = browser.find_element_by_id("email")
password     = browser.find_element_by_id("pass")
confirmation = browser.find_element_by_id("u_0_d_EM") # it is not necessarily that easy with facebook 
username.send_keys("mockdatausername")
password.send_keys("mockdatapassword")
confirmation.click()

wait = WebDriverWait(browser, 5)
page_title = browser.title 

time.sleep(5)
driver.quit()