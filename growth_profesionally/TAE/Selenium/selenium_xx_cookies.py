from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import ActionChains
import time 

PATH = "C:\Program Files (x86)\chromedriver.exe"
ichromeOptions = webdriver.ChromeOptions()
ichromeOptions.binary_location = r"C:\Program Files\BraveSoftware\Brave-Browser\Application\brave.exe"
driver = webdriver.Chrome(PATH, options=ichromeOptions)

driver.get("https://orteil.dashnet.org/cookieclicker/")

cookie = driver.find_element_by_id("bigCookie")
cookie_nr = driver.find_element_by_id("cookies")
upgrade1_name = driver.find_element_by_id("product0")
upgrade1_price = driver.find_element_by_id("productPrice0")
upgrade1_unlocked = False

while True:
    cookie.click()
    time.sleep(.1)
    if cookie_nr > upgrade1_price and not upgrade1_unlocked:
        upgrade1_name.click()
        upgrade1_unlocked = True 
    time.sleep(.1)

time.sleep(5)