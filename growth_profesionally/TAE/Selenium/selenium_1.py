# https://www.pajacyk.pl/ 
# 

from selenium import webdriver
from selenium.webdriver.common.keys import Keys  
from selenium.webdriver.common.by import By 
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC 
import time 

PATH = "C:\Program Files (x86)\chromedriver.exe"
co_instance = webdriver.ChromeOptions()
co_instance.binary_location = r"C:\Program Files\BraveSoftware\Brave-Browser\Application\brave.exe"
driver = webdriver.Chrome(PATH, options=co_instance)

'''
# PROJECT 1 - automate clicking Pajacyk's stomach 

driver.get("https://www.pajacyk.pl/")
# driver.close() # closes the current tab
print (driver.title)
driver.quit() # closes the entire browser 
'''


# PROJECT 2 - following TWT 
print ("Getting the website...")
driver.get("https://www.techwithtim.net/")
print ("GOT the website ; waiting and maximizing...")
time.sleep(1)
driver.maximize_window()
print ("MAXImized")
time.sleep(1)

# search_bar = driver.find_element_by_name("s") # probably layout has changed or sth like this 
search_bar = driver.find_element_by_class_name("search-field")
search_bar.send_keys("test")
search_bar.send_keys(Keys.RETURN)

# EXPLICIT WAITS required - in order to not interact with an object which is not yet loaded (we r in the web, bro)
# main = driver.find_element_by_id("main") # <- it is declared below ; within try except clause  

# EXPLICIT WAITS implemented
try: 
    # main = WebDriverWait(driver, 10).until(
    #     EC.presence_of_element_located(By.ID, "main") # IT REQUIRES TUPLE HERE! mothafucka 
    # )
    main2nd = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CLASS_NAME, "site-main")) 
    )
    print (main2nd.text)

    articles = main2nd.find_elements_by_tag_name("article")

    for article in articles:
        header = article.find_element_by_class_name("entry-title")
        print (header.text)

except:
    print ("SOMETHING IS FUCKING NO YES!")
    driver.quit()

finally:
    time.sleep(2)
    print ("SEE YA!")
    driver.quit() # closes the browser

#### 

'''
- when we access elements, we need to access them by a few different properties ; or one of the few properties
    THIS IS THE PROPER ORDER TO FOLLOW BY ; to search through by ; first go for id, then name, afterwards class 
    - id - guaranteed to be unique in HTMLs ; therefore the best option 
    - name 
    - class ; issue is the Selenium looks for the first element from the top ; first occurence of such an element ; if we have multiple objects of specific class... we might get confused
    - tag 

- HTTP - hypertext transfer protocl ; application layer 
'''

# https://www.programcreek.com/python/example/100025/selenium.webdriver.ChromeOptions ; some useful examples of preconfigurating Selenium 