#!/bin/bash

# input in bash scripts is passed using 'read' keyword 
# read myname 
# echo "Hello" $myname

<<wtf 
comment1, ladida 
comment2, line 2 to be precise
comment3, ladida as well 
wtf

# read my_second_name 
# echo "Welcome" $my_second_name

# sleep 2  # works fine 

# https://tecadmin.net/tutorial/bash-comments

: '
looping 
apparently it works as well 
even though notepad++ does not recognize it as a command
' 


for i in {1..50}
do 
	echo $i
	# sleep 0.1
done

# sleep 2

read x 
read y 

sum=x+y

echo sum # ale pojebane, lol, jak wypisać wartość tej zmiennej? TODO 
sleep 2


