import unittest
# from unittest.mock import Mock 
from unittest.mock import MagicMock 
from unittest.mock import patch 

'''
    ##### BASICS #####
# mock = Mock()

# print(mock)
# print(mock())
# # == 
# print(mock.return_value)

# #### 

# stuntman = Mock() 
# stuntman.jump_off_building
# stuntman.light_on_fire 

    ##### BASICS #####
'''

'''
    ##### artificial, not understood yet stuff #####

three_item_list = MagicMock()
three_item_list.pop.side_effect = [3, 2, 1, IndexError("pop from empty list")]

print(three_item_list.pop()) # 3
print(three_item_list.pop()) # 2 
print(three_item_list.pop()) # 1
# print(three_item_list.pop()) # IndexError with its description 

filepaths_return = MagicMock() 

class MockCallsTest(unittest.TestCase):
    def test_mock_calls(self):
        mock = MagicMock()
        mock() # invoking mock 
        mock.assert_called() # this asserts that mock was called at least once ; it was - one line higher, so test is OKAY 

    def test_not_called(self):
        mock = MagicMock() # brand new MagicMock object - independent from one above 
        mock.assert_not_called() # OK, mock is not invoked 

    def test_called_with(self):
        mock = MagicMock()
        mock(1, 2, 3)
        mock.assert_called_with(1, 2, 3) # OK ; has to be a complete match 

    def test_mock_attributes(self):
        mock = MagicMock()
        mock()
        mock(1, 2)

    ##### artificial, not understood yet stuff #####    
'''
    

if __name__ == "__main__":
    # unittest.main()

    # tworząc mocka w ten sposób zakładam, że enlist_path_mock działa poprawnie - mam prawo tak założyć, bo funkcja jest przetestowana 
    enlist_path_mock = MagicMock() 
    enlist_path_mock.return_value = ['C:\\Users\\rsimo\\Desktop\\ZF_alten\\repo\\alten_zf_kafas\\production_eol\\basic_test\\production_eol_npHIL_2020_10_06_14h37m06s.html', 
    'C:\\Users\\rsimo\\Desktop\\ZF_alten\\repo\\alten_zf_kafas\\production_eol\\basic_test\\production_eol_npHIL_2020_10_06_14h37m06s.xml', 
    'C:\\Users\\rsimo\\Desktop\\ZF_alten\\repo\\alten_zf_kafas\\production_eol\\basic_test\\production_eol_npHIL_2020_10_06_14h37m06s_get_line_image_dv.png', 
    'C:\\Users\\rsimo\\Desktop\\ZF_alten\\repo\\alten_zf_kafas\\production_eol\\basic_test\\production_eol_npHIL_2020_10_06_14h37m06s_get_line_image_sfr.png', 
    'C:\\Users\\rsimo\\Desktop\\ZF_alten\\repo\\alten_zf_kafas\\production_eol\\basic_test\\production_eol_npHIL_2020_10_06_14h37m06s_sfr_plot_main.png', 
    'C:\\Users\\rsimo\\Desktop\\ZF_alten\\repo\\alten_zf_kafas\\production_eol\\basic_test\\production_eol_npHIL_2020_10_06_14h37m06s_sp_plot_main_0.png', 
    'C:\\Users\\rsimo\\Desktop\\ZF_alten\\repo\\alten_zf_kafas\\production_eol\\basic_test\\production_eol_npHIL_2020_10_06_14h37m06s\\TC_Prod_001_001_2020_10_06_14h50m19s.pcap.gz', 
    'C:\\Users\\rsimo\\Desktop\\ZF_alten\\repo\\alten_zf_kafas\\production_eol\\basic_test\\production_eol_npHIL_2020_10_06_14h37m06s\\TC_Prod_002_001_2020_10_06_14h50m37s.pcap.gz', 
    'C:\\Users\\rsimo\\Desktop\\ZF_alten\\repo\\alten_zf_kafas\\production_eol\\basic_test\\production_eol_npHIL_2020_10_06_14h37m06s\\TC_Prod_003_001_2020_10_06_14h50m55s.pcap.gz', 
    'C:\\Users\\rsimo\\Desktop\\ZF_alten\\repo\\alten_zf_kafas\\production_eol\\basic_test\\production_eol_npHIL_2020_10_06_14h37m06s\\TC_Prod_004_001_2020_10_06_14h51m46s.pcap.gz', 
    'C:\\Users\\rsimo\\Desktop\\ZF_alten\\repo\\alten_zf_kafas\\production_eol\\basic_test\\production_eol_npHIL_2020_10_06_14h37m06s\\TC_Prod_005_001_2020_10_06_14h52m06s.pcap.gz', 
    'C:\\Users\\rsimo\\Desktop\\ZF_alten\\repo\\alten_zf_kafas\\production_eol\\basic_test\\production_eol_npHIL_2020_10_06_14h37m06s\\TC_Prod_006_001_2020_10_06_14h52m25s.pcap.gz', 
    'C:\\Users\\rsimo\\Desktop\\ZF_alten\\repo\\alten_zf_kafas\\production_eol\\basic_test\\production_eol_npHIL_2020_10_06_14h37m06s\\TC_Prod_007_001_2020_10_06_14h52m50s.pcap.gz', 
    'C:\\Users\\rsimo\\Desktop\\ZF_alten\\repo\\alten_zf_kafas\\production_eol\\basic_test\\production_eol_npHIL_2020_10_06_14h37m06s\\TC_Prod_008_001_2020_10_06_14h53m43s.pcap.gz'
    ]

    ep_function_mock = MagicMock()
    ep_function_mock.return_value = ["path1\\file1.txt", "path22\\file223.txt", "path3\\file345.txt"]

    # print(ep_function_mock()) returns output ; prints output ; USE THIS WAY 
    
    

'''
    ##### DOCUMENTATION EXAMPLE #####
        does not work because of undefinied module 

@patch('module.ClassName2')
@patch('module.ClassName1')

# The patch() decorator/context manager makes it easy to mock classes or objects in a module under test 
    # so 'module' should be replaced with the name of the module under test... 

# The object you specify will be replaced with a mock (or other object) during the test and restored when the test ends 
    # Q1: where do I specify an object? 
    # Q2: where do I specify a mock? 

def test(MockClass1, MockClass2):
    module.ClassName1()
    module.ClassName2()

    assert MockClass1 is module.ClassName1 # TODO answer for Q2? 
    assert MockClass2 is module.ClassName2 
    assert MockClass1.called
    assert MockClass2.called

test()

# https://docs.python.org/3/library/unittest.mock.html#quick-guide

    ##### DOCUMENTATION EXAMPLE #####
'''