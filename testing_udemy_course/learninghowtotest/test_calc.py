import unittest 
import calc 
import pdb # python debugger 

class TestCalc(unittest.TestCase):

    def test_add_return(self):
        result = calc.add(10, 12)
        self.assertEqual(result, 22)
        self.assertEqual(calc.add(1, -1), 0)
        self.assertEqual(calc.add(-1, -1), -2)
        self.assertNotEqual(calc.add(12, 88), 101)

    def test_add_type(self):
        intresult = calc.add(10, 12)
        self.assertEqual(isinstance(intresult, int), True)
        floatresult = calc.add(22.5, 22.4)
        # pdb.set_trace() 
        # print(self.assertEqual(isinstance(floatresult, float), True))
        self.assertEqual(isinstance(floatresult, float), True)
        self.assertEqual(isinstance(floatresult, float), True)
        self.debug()
        pdb.set_trace() 

    def test_subtract_return(self):
        pass

    def test_division_return(self):
        pass

if __name__ == '__main__':
    unittest.main()