def add(a, b):
    #assert(a, type(int))
    assert isinstance(a, int) and isinstance(b, int), "Both arguments must be integers"
    # if line above is not getting a True 
    # program is breaking!
    # quote after comma is a description 
    # if line 3 is okay, it evaluates to None #pr0 

    # you can add exception handling here ; just to practice and therefore become even more skillful 

    return a + b 

print(add(3, 5))
# print(add(3, '5')) # TypeError 

