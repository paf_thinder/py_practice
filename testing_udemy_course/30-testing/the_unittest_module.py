import unittest 

class TestStringMethods(unittest.TestCase):

    def test_split(self):
        exstring1 = "Stefano Okaka Chuka" # example string 1, test spaces 
        exstring2 = "Marc-Andre ter Stegen" # example string 2, test dash 
        exstring3 = "This_is_new_way_of_formatting_strings" # example string 3, test underscore 
        exstring4 = "Hello, my name is Simon, I am buddhaXbadass" # example string 4, affirmation, test comma 

        # ONE ASSERTION PER TEST IS ENOUGH
        self.assertEqual(exstring1.split(), ["Stefano", "Okaka", "Chuka"])
        # ONE ASSERTION PER TEST IS ENOUGH
        # make sure that your test is focused on one thing and one thing only 
        # each test should server to isolate one specific bit of functionality 

        # self.assertEqual(exstring2.split('-'), ["Marc", "Andre ter Stegen"])
        # self.assertEqual(exstring3.split('_'), ["This", "is", "new", "way", "of", "formatting", "strings"])
        # self.assertEqual(exstring4.split(','), ["Hello", "my name is Simon", "I am buddhaXbadass"])
        # these are redundant 

        # to test 2nd parameter of the function - check the length of the list 

    def test_count(self):
        pass 


if __name__ == "__main__":
    unittest.main()