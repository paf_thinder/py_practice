import calculator as calc 

from calculator import creator 
from calculator import subtract
from math import sqrt # no need to use namespace 
# no math.sqrt ; but just sqrt 

print(calc.add(5, 3))

print(subtract(5, 3))
print(creator) # why am I creating my own reality in a way I want it to be? 

print(sqrt(9))