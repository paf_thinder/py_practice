creator = "SimON von Boss"
PI = 3.14

def add(a, b):
    return a + b

def subtract(a, b):
    return a - b

def area(radius):
    return radius * radius * PI

_year = 2020 