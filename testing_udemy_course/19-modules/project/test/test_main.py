import unittest 

def add(a, b):
    return a + b

# testing add function in a loop 

class Test_add(unittest.TestCase):

    def test_add_in_the_loop(self):
        for i in range(5):
            ret = add(10, i)
            self.assertTrue(ret, 10 + i)