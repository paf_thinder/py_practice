import calculator
import math

print("This thing higher is coming from calculator.py")
# print(add(1,55))

print(calculator.add(5, 55))

# practicing hard accessing variables from MODULE 
print("Who da fuck is the creator? {}".format(calculator.creator))
print("Who's da boss? {}".format(calculator.creator))
print("Who's the ladiesman? {}".format(calculator.creator))

# make this variable more concise 
# b stands for boss 
b = calculator.creator
print("{}! {}! {}!\n \nWHO WILL WIN? {}\n  WHO? {}\n   WHO? {} {} {}!".format(b, b, b, b, b, b, b, b))
print("Do boju, do boju, wygrywam symulację! WindakBoss!")

# calculator.calculator is valid to use in main.py 
# print(calculator.calculator)

# what is returned if you pass the imported calculator module into the type function? 
print(type(calculator)) # <class 'module'>

print("###\nMATHEMATICS: ")
print(dir(math)) # list of functions, nice! and variables! 

print("###\nCALCULATOR meanwhile: ")
print(dir(calculator))