import os 

'''
    #### OS.WALK() PRACTICE ####

filepaths = []
#input_path = r"C:\Users\rsimo\Desktop\ZF_alten\repo\alten_zf_kafas\production_eol" # too complicated atm, let's make it simpler 
input_path = r"C:\Users\rsimo\Desktop\devOps\py_practice\code"

iter = 0 
for root, _, files in os.walk(input_path):
    print("Variable filepaths equals to: {}".format(filepaths))
    print("\n####\n")
    # debugger is also kinda hard, gotta practice
    if (iter == 0):
        print("Type of variable root is: {}".format(type(root)))    
        print("Type of variable _ is: {}".format(type(_)))    
        print("Type of variable files is: {}".format(type(files)))    

    print("Variable root equals to: {}".format(root))
    print("Variable _ equals to: {}".format(_))
    print("Variable files equals to: {}".format(files))

    filepaths.extend([os.path.join(root, f) for f in files])

    iter = 1 

    # root - cwd
    # _ - all folders in cwd 
    # files - all files in cwd 

    # Python method walk() generates the file names in a directory tree by walking the tree either top-down or bottom-up
    # https://www.tutorialspoint.com/python/os_walk.htm

    #### OS.WALK() PRACTICE ####
'''