class Card():

    SUITS = ("Hearts", "Clubs", "Spades", "Diamonds")
    RANKS = (
            "2", "3", "4", "5", "6", "7", "8", "9", "10", 
            "Jack", "Queen", "King", "Ace"
            )

    def __init__(self, rank, suit):
        self.rank = rank
        self.suit = suit 
    
    def name_the_card(self):
        ret = self.rank + " of " + self.suit 
        return ret

    # exactly the same output as function name_the_card defined above will be given by 
    def __str__(self):
        return f"{self.rank} of {self.suit}"
        # this allows to do print(Card("7", "Spades")) ; and it does print the return

    def __repr__(self):
        return f"Card('{self.rank}', '{self.suit}')"    
        # this prints representation once object is instantiated 



# #### 

c = Card("2", "Clubs")
d = Card("Queen", "Hearts")

print(c.name_the_card())
print(d.name_the_card())

print(c.__str__())
print(d.__str__())


