import unittest 
from poker.card import Card 

class CardTest(unittest.TestCase):

    def test_has_rank(self):
        # self.assertEqual(1, 1)
        card = Card(rank = "Queen", suit = "Hearts") 
        self.assertEqual(card.rank, "Queen")

    def test_has_suit(self):
        card = Card(rank = "2", suit = "Clubs")
        self.assertEqual(card.suit, "Clubs")

        # part of reliability is consistency 
        # let's keep ranks as string 

    def test_rank_is_valid(self):
        valid_rank = ["2", "3", "4", "5", "Queen", "King"] # TODO fill this list 
        card_zwei = Card("4", "Clubs")
        rank_valid = card_zwei.rank in valid_rank
        self.assertEqual(rank_valid, True)

    def test_suit_is_valid(self):
        valid_suit = ["Hearts", "Clubs", "Spades", "Diamonds"]
        card_drei = Card("King", "Clubs")
        suit_valid = card_drei.suit in valid_suit
        self.assertEqual(suit_valid, True)

    def test_has_string_representation_with_rank_and_suit(self):
        card_vier = Card("5", "Diamonds")
        # self.assertEqual(str(card_vier), "5 of Diamonds") # that won't work 
        self.assertEqual(card_vier.name_the_card(), "5 of Diamonds")
        self.assertEqual(card_vier.__str__(), "5 of Diamonds") # tests the same thing as above ; redundancy for learning purpose

    def test_has_technical_representation(self):
        card_vier = Card("5", "Diamonds")
        self.assertEqual(repr(card_vier), "Card('5', 'Diamonds')")

    def test_card_has_4_possible_suit_options(self):
        self.assertEqual(
            Card.SUITS, 
            ("Hearts", "Clubs", "Spades", "Diamonds")
        )

    def test_card_has_13_possible_rank_options(self):
        self.assertEqual(
            Card.RANKS,
            (
                "2", "3", "4", "5", "6", "7", "8", "9", "10",
                "Jack", "Queen", "King", "Ace"
            )
        )


if __name__ == '__main__':
    unittest.main()