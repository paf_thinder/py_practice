import unittest

# TODO - dobra, totalnie nie wiem dlaczego ten import nie działa ; moduł IMPORT do opanowania 
# from poker import product 

def product(a, b):
    return a * b  

# class TestProduct is my name
# it inherites from unittest.TestCase
class TestProduct(unittest.TestCase): 

    def test_multiplies_two_numbers(self):
        self.assertEqual(product(3, 5), 15)
        self.assertNotEqual(product(2, 4), 12)

    def test_negative_multiplying(self):
        # - * - 
        self.assertEqual(product(-4, - 12), 48)
        self.assertNotEqual(product(-2, -21), -42)

        # - * +
        self.assertEqual(product(1, -100), -100)
        self.assertNotEqual(product(100, -2), 200)


    def test_multiplication_by_zero(self):
        self.assertEqual(product(0, 200), 0)
        self.assertNotEqual(product(0, 2004), 1)


if __name__ == '__main__':
    unittest.main()

# every new error is a sign of progress 
# jak z jazdą na nartach Katii 
# i z problemami w SpaceX u Muska 