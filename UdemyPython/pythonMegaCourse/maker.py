#!/bin/python3 

from typing import final

def maker():
    # Prepare user input for further processing ; lower - to make sure that questions list is valid to use (utilize)
    user_input = input("Say something: ")
    # user_input = user_input.lower() # TODO it does not work for city names ; example: What is the weather in Turin? # let's assume user is sane 
    # otherwise I would have to put the list of all cities in the word and check every word if it is an element of this list 

    # capitalize first letter 
    user_input = user_input[0].upper() + user_input[1:]

    final_msg = ""
    questions = ["What", "How", "Where", "When", "Who", "Whom", "Whose", "Why"]

    # until 'secret word' is passed keep asking 
    while user_input != "\end":

        # check if the sentence is question == if first word is a 'question word', assume it is a question
        # TODO "What?" - does not work for this line 

        if user_input.split(" ")[0] in questions:
            # TODO it may be too naive approach ; question can be also: So... what is the weather in Turin? ; so why do you think you will become a millionaire? 
            # if user already added question mark, do not duplicate it ; if last sign is not a question mark - add it 
            
            # if last char of the sentence is space - get rid of it 
            if user_input[-1] == " ":
                user_input = user_input[:-1]

            if user_input[-1] != "?":
                # print (user_input[-1]) # lol, habit of putting space at the end of line 
                user_input += "?"

        # TODO huh this is much fucking harder then I would expect it to be. If it is a question - put a question mark 
        # if not - add the dot at the end 
        # FUCKING SHIT - if it is a question 1 branch ; if not - 2nd branch ; do not fucking go into two branches of the if statement in the same time! 
        else:
            user_input = user_input[:-1]
            user_input += "."

        # elif user_input[-1] != ".":
        #         # user_input[-1] = "." # str object does not support item assignment 
        #         user_input = user_input[:-1]
        #         user_input += "."

        final_msg += user_input
        break

    print (final_msg)

def capitalizeFirstLetter(user_input):
    # self-describing, isn't it? #good_practices 
    pass

def addQuestionMark(user_input):
    # add question mark at the end of the sentence
    pass

def isQuestion(user_input):
    # determine if the user input is a question  

    # last character is a questionmark
    if user_input[-1] == "?":
        return True

    # last character is a space - get rid of it and check if the last char is a questionmark 
    if user_input[-1] == " ":
        if user_input[:-2] == "?":
            return True 

    # includes word from 'questions' array 

def calmDownTheUser(user_input):
    # if there are multiple exclamation or question marks - get rid of those and leave only one ; we are gentlemen, are not we? == we're gentlemen, aren't we? 
    # TODO - if the user is fucking moron and puts multiple question marks at the end of the sentence - get rid of those! 
    pass

def removeTrailingWhitespaces(user_input): 
    # let's check if there are whitespaces in the first place 
    whitespacesPresent = False 

    if user_input[-1] == " ":
        whitespacesPresent = True 
        # there is some job to be done 

    if whitespacesPresent:
        # iterate from the end of the word until the char is different then space 
        i = 0
        for char in user_input[::-1]:
            if char == " ":
                i += 1
            else:
                break

        user_input = user_input[:-i] # shortening string by i signs 
    
    return user_input
    
    # test: print(removeTrailingWhitespaces("how can I test it better?        ") + " I will find it out soon!")

if __name__ == "__main__":
    # main() # main is not defined 
    # maker() 
    pass

