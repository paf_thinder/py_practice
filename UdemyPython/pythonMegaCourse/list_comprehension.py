temps = [221, 231, 443, 340, -9999]

new_list = []
for temp in temps:
    new_list.append(temp / 10)

print (new_list)

#### 

# same output will de delivered thanks to this: 
new_list2 = [temp / 10 for temp in temps if temp > -50] # 3 lines in 1. codegolf! 
print (new_list2)

#### 

a = ["ab", "OoO", "bad" "be", "um"]
#a = [sound + "c" for sound in a]
print (a)

#### 

# function ; takes list as an input ; list contain both strings and ints 
# remove strings 
def foo(lst):
    return [i for i in lst if isinstance(i, int)] # very pythonic. gg! 

#### 

# if else in list comprehension changes order of elements 

new_list3 = [temp / 10 if temp > -50 else 0 for temp in temps] # for goes to the end
a2 = [sound + "c" if len(sound) == 2 else sound + "OoO" for sound in a]
print (a2)

def foo2(lst):
    return [i if isinstance(i, int) else 0 for i in lst]

print (foo2(["asdf", 11, "asdf", 10, "xys"]))

#### 

def foo(lst):
    return sum([float(i) for i in lst]) # returns sum of the list ; original input is a list containing strings ; convert those to float and add every element of the list 

def defaultParameters(a, b = 5):
    return a + b

print(defaultParameters(11))