import unittest 
from maker2 import removeTrailingSpace

class TestMakerSubFunctions(unittest.TestCase):
    '''
    Tests functions which are the part of maker function
    '''
    def test_removingTrailingSpaces(self):
        func_ret = removeTrailingSpace("abc   ")
        expected_ret = 3
        self.assertEqual(len(func_ret), expected_ret)

if __name__ == "__main__":
    unittest.main()