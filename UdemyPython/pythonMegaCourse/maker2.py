# section 8 

def removeTrailingSpace(strinput):
    # TODO do it better, for more than one space 
        # find last char 
        # count nr of trailing spaces 
        # return strinput[:-nr_of_trailing_spaces]

    # for c in strinput:
        # NIE WIEM KURWA, NIE WIEM JAK TO ZROBIĆ PO BOŻEMU # JA JEBIE :( 
        # edit: jakoś poszło, jakoś poszło ;]] 
    if strinput[-1] == " ":
        c = strinput[-1]
        iter = 0
        while c == " ":
            iter += 1 
            c = strinput[-1 - iter]
        #print ("nr of trailing spaces: {}".format(iter))
        return strinput[:-iter]
    else:
        #print ("no trailing spaces")
        return strinput

    

    # current, working implementation
    # if strinput[-1] == " ":
    #     return strinput[:-1]
    # else:
    #     return strinput

def isQuestion(strinput):
    questions = ["what", "how", "where", "when", "who", "whom", "whose", "why"]
    strinput = strinput.lower()
    for word in strinput.split(" "):
        if word in questions:
            return True

    # you can use also startswith() function 
    # startswith(tuple here)
    # "somestring ladida".startswith(("som", "placeholder", "remember this is tuple, if it is only 1-element, no need to put brackets")) # returns True 

def isStatement(strinput):
    # simple assumption here #kiss
    if not isQuestion(strinput):
        return True

def capitalizeFirstLetter(strinput):
    return strinput[0].upper() + strinput[1:]

def adjustingSentence(strinput):
    strinput = capitalizeFirstLetter(strinput)
    # write tests including trailing whitesigns
    if isQuestion(strinput):
        return strinput + "?"
    else:
        return strinput + "."

# approach from the instructor 
def sentenceMaker(phrase):
    interrogatives = ("how", "what", "why", "Why")
    capitalized = phrase.capitalize() 
    if phrase.startswith(interrogatives):
        return "{}?".format(capitalized)
    else:
        return "{}.".format(capitalized)

    # simpler, more elegant. way more elegant :) 
    # why do I learn from the best? 

# my approach 
def main():
    user_input = ""
    ret = ""

    while user_input != "\end":
        user_input = input("Say something: ")
        temp = user_input
        temp = removeTrailingSpace(temp)
        temp = adjustingSentence(temp)
        ret += temp + " "

    # print (ret[:-5]) # we don't need \end to be printed ; and the last space 
    print (removeTrailingSpace(ret)[:-5]) # no \end nor the trailing spaces to be printed 

# more elegant, instructors approach
def main2():
    results = []
    while True:
        user_input = input("Say something: ")
        if user_input == "\end":
            break
        else:
            results.append(sentenceMaker(user_input))

    # print (results)
    # BUENO 

    # my approach
    # ret = ""
    # for sentence in results:
    #     ret += sentence + " "

    # return ret[:-1] # no need trailing space 

    # pythonic approach 
    return " ".join(results)

if __name__ == "__main__":
    # main()
    print(main2()) # haha, but this solution doesn't use my clever removeTrailingSpace() ;d 