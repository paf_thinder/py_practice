import math

class Point:

    def __init__(self, x, y):
        self.x = x
        self.y = y 

    def falls_within_rectangle(self, lowerleft, upperright):
        cond1 = self.x > lowerleft[0] and self.x < upperright[0]
        cond2 = self.y > lowerleft[1] and self.y < upperright[1]
        return True if (cond1 and cond2) else False 

    def distance_from_point(self, point):
        return math.sqrt((point.x - self.x)**2 + (point.y - self.y)**2)

    # def distance_from_point(self, x, y):
        # return math.sqrt((x - self.x)**2 + (y - self.y)**2)
        # self.x_distance = x - self.x  # yep, that was naive ;] 
        # self.y_distance = y - self.y 
        
class Rectangle:

    # x, y are the coordinates of left bottom corner 
    def __init__(self, x, y, side_length):
        self.x = x 
        self.y = y 
        self.side_length = side_length

    def calculate_area(self):
        return self.side_length ** 2

    def get_tips_coordinates(self):
        self.bl = [self.x, self.y]  # bottom left
        self.ul = [self.x, self.y + self.side_length] 
        self.br = [self.x + self.side_length, self.y]
        self.ur = [self.x + self.side_length, self.y + self.side_length]  # upper right 

        return [self.bl, self.ul, self.br, self.ur]