# import temp  # this doesn't allow us to declare a point explicitly ; u have to do this like this: x = temp.Point()
from temp import Point  # this allows to instantiate like this: instance_of_point = Point()
from temp import Rectangle

if __name__ == "__main__":
    instance_of_point = Point(5, 10)
    point2 = Point(25, 30)
    print(type(instance_of_point))  # class 'temp.Point'

    print(instance_of_point.falls_within_rectangle([0, 0], [6, 12]))
    print(instance_of_point.distance_from_point(point2))
    # print(instance_of_point.distance_from_point(25, 30))

    print (Rectangle(0, 0, 10).get_tips_coordinates())