class Card():
    SUITS = ("Hearts", "Clubs", "Spades", "Diamonds")
    RANKS = (
                "2", "3", "4", "5", "6", "7", "8", "9", "10",
                "Jack", "Queen", "King", "Ace"
            )


    def __init__(self, rank, suit):
        if rank not in self.RANKS:
            raise ValueError(f"Invalid rank: {rank} ; it is not included within RANKS: {self.RANKS}")

        if suit not in self.SUITS:
            raise ValueError(f"Invalid suit: {suit} ; it is not included within SUITS: {self.SUITS}")

        self.rank = rank 
        self.suit = suit 

    def __str__(self):
        return f"{self.rank} of {self.suit}"

    def __repr__(self):
        return f"Card('{self.rank}', '{self.suit}')" # this pops out when you want to see what the object is 
        
        # for example 
        # example_instance = Card("5", "Clubs")
        # in terminal: example_instance 
        # output is return of this function: Card('5', 'Clubs')