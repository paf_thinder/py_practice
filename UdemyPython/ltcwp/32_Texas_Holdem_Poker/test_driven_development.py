import unittest

def product(a, b):
    # ret = 0
    # for x in range(b):
    #     ret += a

    # return ret 

    # after refactoring
    return a * b

class TestProduct(unittest.TestCase):
    def test_multiplies_two_numbers_together(self):
        self.assertEqual(
            product(3, 5), 
            15
        )

if __name__ == "__main__":
    unittest.main()