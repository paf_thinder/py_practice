a = 3 
b = a 

a = 5 
# b - still 3 # b points to the OBJECT of 3 

####

alist = [1, 2, 3]
blist = alist

blist.append(4)
print (alist) # 1 2 3 4 

alist.append(100)
print (blist) # 1 2 3 4 100 

####

# MUTABLE TYPES
list = []
dict = {}

# IMMUTABLE TYPES 
set = {}
tuple = ()